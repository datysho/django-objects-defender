# Django Undeletable
- - -

# Installation:
1. pip install django-objects-defender
2. add 'objects_defender' to the end of INSTALLED_APPS
3. add DEFENDED_OBJECTS to settings.py like:

### Simple usage ###
```
#!python

DEFENDED_OBJECTS = {
    'main.Page': [
        {'slug': ['main']},  # defend all Page objects which slug is 'main'
    ],
}
```

### Advanced usage ###
```
#!python

DEFENDED_OBJECTS = {
    'main.Page', [
        {'slug': ['asdf', 'qwerty']}  # defend all Page objects which slug is 'asdf' or 'qwerty'
    ], 
    'main.Post', [
        {'author__name': ['Pupkin']}  # defend all Post objects which author name is 'Pupkin'
    ],
    'main.Message', [
        {'author__name': ['Vasya']},  # defend all Message objects which author name is 'Vasya'
        {'author__email': ['e@e.com'}  # or author email is 'e@e.com'
    ],
    'main.Comment', [
        {'id__gt': [1]}  # defend all Comment objects which id greater then 1
    ],
}
```