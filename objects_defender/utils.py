from django.conf import settings
from django.db.models.loading import get_model
from operator import or_
from django.db.models import Q


defended_models = {}
for model, params in settings.DEFENDED_OBJECTS.iteritems():
    defended_models[get_model(model)] = params


def get_filters(klass):
    return [Q(**{fld: val}) for optns in defended_models[klass] for fld, values in optns.iteritems() for val in values]


def get_defended_fields(klass, exclude=('id',)):
    defended_fields = []
    for field in (field for optns in defended_models[klass] for field, values in optns.iteritems()):
        if field not in exclude:
            defended_fields.append(field.split('__')[0] if '__' in field else field)
    return tuple(set(defended_fields))


def is_deletable(instance):
    sender = instance.__class__
    return instance not in sender.objects.filter(reduce(or_, get_filters(sender)))
