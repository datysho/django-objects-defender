from operator import or_
from django.contrib import admin
from django.contrib import messages
from django.contrib.admin.actions import delete_selected
from .utils import defended_models, get_filters, is_deletable, get_defended_fields


def defended_delete_selected(modeladmin, request, queryset):
    model = queryset.model
    if model in defended_models:
        queryset = queryset.exclude(reduce(or_, get_filters(model)))
        if not queryset.exists():
            messages.add_message(request, messages.DEFAULT_LEVELS['WARNING'], "This object couldn't be deleted")
            return None
    return delete_selected(modeladmin, request, queryset)
defended_delete_selected.short_description = delete_selected.short_description
admin.site._actions['delete_selected'] = defended_delete_selected


class DefendedAdminMixin(object):
    def has_delete_permission(self, request, obj=None):
        if obj:
            return is_deletable(obj)
        return super(DefendedAdminMixin, self).has_delete_permission(request, obj)

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super(DefendedAdminMixin, self).get_readonly_fields(request, obj)
        if obj and not is_deletable(obj):
            return readonly_fields + get_defended_fields(obj.__class__)
        return readonly_fields

    def get_prepopulated_fields(self, request, obj=None):
        prepopulated_fields = super(DefendedAdminMixin, self).get_prepopulated_fields(request, obj)
        if obj and not is_deletable(obj):
            prepopulated_fields = prepopulated_fields.copy()
            for field in get_defended_fields(obj.__class__):
                if field in prepopulated_fields:
                    del prepopulated_fields[field]
        return prepopulated_fields

    def save_form(self, request, form, change):
        obj_new = super(DefendedAdminMixin, self).save_form(request, form, change)
        obj_old = form.cleaned_data.get('id')
        if obj_old and obj_old.__class__ in defended_models and not is_deletable(obj_old):
            for field in get_defended_fields(obj_old.__class__):
                setattr(obj_new, field, getattr(obj_old, field))
                messages.add_message(request, messages.DEFAULT_LEVELS['WARNING'],
                                     "Field '{field}' couldn't be edit".format(field=field))
        return obj_new

for model in admin.site._registry:
    if model in defended_models:
        ModelAdmin = admin.site._registry[model].__class__
        DefendedAdmin = type(str(model.__class__) + 'DefendedAdmin', (DefendedAdminMixin, ModelAdmin,), {})

        admin.site.unregister(model)
        admin.site.register(model, DefendedAdmin)
